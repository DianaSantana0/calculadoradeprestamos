<!-- Diana Santana Sanchex 20 Abril 2017 -->
<script src="http://code.jquery.com/jquery-latest.js"></script>
             <script type="text/javascript">
                 $(document).ready(function () {
                     $('#div2').hide();
                     $('.request-form-bottomtext-2').hide();
                     $('#id_radio1').click(function () {
                     $('#div2').hide();
                     $('#div1').show();
                     $('.request-form-bottomtext-1').show();
                     $('.request-form-bottomtext-2').hide();
                });
                $('#id_radio2').click(function () {
                      $('#div1').hide();
                      $('#div2').show();
                      $('.request-form-bottomtext-1').hide();
                      $('.request-form-bottomtext-2').show();
                 });
               });
</script>

<div id="request-form" class="container-slider">
         <div class="contenedor-inputs-slider">
          <h6>SIMULADOR DE PRÉSTAMOS PARA PARTICULARES</h6>
             <div class="contenedor">
                 <div class="contenedores">
                           <input id="id_radio1" type="radio" name="name_radio1" value="value_radio1" checked="checked"><label for="id_radio1"><span><span></span></span>Calcula tu prestamo</label>
                 </div>
                 <div class="contenedores">
                     <input id="id_radio2" type="radio" name="name_radio1" value="value_radio2"><label for="id_radio2"><span><span></span></span>Calcula tu cuota</label>
                 </div>
             </div>
             <div class="" id="div1">
                 <div class="inputs-slider">
                       <div id="request-group-1" class="request-group">
                 <div class="request-row">
                     <div class="request-question">Cuota:</div>
                     <div class="request-counter">100,000</div>
                     <!-- <input type="text" id="counter-1" class="range-counter" for="range-1" /> -->
                 </div>

                 <div class="request-row">

                     <div class="request-range">
                         <div class="request-range-bg-filled request-range-bg"></div>
                         <div class="request-range-bg-clear request-range-bg"></div>
                         <input id="request-range-amount" class="request-range-input" type="range" min="50000" max="500000" step="2500" value="100000">
                         <!-- <div class="input-range" id="range-1" min="50000" max="500000" step="2500" value="100000"></div> -->
                     </div>
                     <div style="clear: both"></div>
                 </div>
                    </div>
                 </div>
                     <div class="inputs-slider">

                 <div id="request-group-2" class="request-group">
                     <div class="request-row">
                         <div class="request-question">Meses:</div>
                         <div class="request-counter">12</div>
                         <div style="clear: both"></div>
                     </div>
                     <div class="request-row">
                         <div class="request-range">
                             <div class="request-range-bg-filled request-range-bg"></div>
                             <div class="request-range-bg-clear request-range-bg"></div>
                             <input id="request-range-term" class="request-range-input" type="range" min="8" max="48" step="1" value="3">
                         </div>
                         <div style="clear: both"></div>
                     </div>
                 </div>
                 </div>
                 <div class="inputs-slider">

             <div id="request-group-3" class="request-group">
                 <div class="request-row">
                     <div class="request-question">Tipo:</div>
                     <div class="request-counter">12</div>
                     <div style="clear: both"></div>
                 </div>
                 <div class="request-row">
                     <div class="request-range">
                         <div class="request-range-bg-filled request-range-bg"></div>
                         <div class="request-range-bg-clear request-range-bg"></div>
                         <input id="request-range-interest" class="request-range-input" type="range" min="2" max="15" step="1" value="2">
                     </div>
                     <div style="clear: both"></div>
                 </div>
             </div>

             </div>
        </div>

        <!--Div2 Calcular Cuota--->
        <div class="" id="div2">
            <div class="inputs-slider">
                  <div id="request-group-cuota-1" class="request-group">
            <div class="request-row">
                <div class="request-question">Importe:</div>
                <div class="request-counter">100,000</div>
                <!-- <input type="text" id="counter-1" class="range-counter" for="range-1" /> -->

            </div>

            <div class="request-row">
                <div class="request-range">
                    <div class="request-range-bg-filled request-range-bg"></div>
                    <div class="request-range-bg-clear request-range-bg"></div>
                    <input id="request-range-amount" class="request-range-input" type="range" min="50000" max="500000" step="2500" value="100000">
                    <!-- <div class="input-range" id="range-1" min="50000" max="500000" step="2500" value="100000"></div> -->
                </div>
                <div style="clear: both"></div>
            </div>
        </div>
            </div>
                <div class="inputs-slider">

            <div id="request-group-cuota-2" class="request-group">
                <div class="request-row">
                    <div class="request-question">Meses:</div>
                    <div class="request-counter">12</div>
                    <div style="clear: both"></div>
                </div>
                <div class="request-row">
                    <div class="request-range">
                        <div class="request-range-bg-filled request-range-bg"></div>
                        <div class="request-range-bg-clear request-range-bg"></div>
                        <input id="request-range-term" class="request-range-input" type="range" min="3" max="48" step="1" value="20">
                    </div>
                    <div style="clear: both"></div>
                </div>
            </div>
            </div>
            <div class="inputs-slider">
        <div id="request-group-cuota-3" class="request-group">
            <div class="request-row">
                <div class="request-question">Tipo:</div>
                <div class="request-counter">12</div>
                <div style="clear: both"></div>
            </div>
            <div class="request-row">
                <div class="request-range">
                    <div class="request-range-bg-filled request-range-bg"></div>
                    <div class="request-range-bg-clear request-range-bg"></div>
                    <input id="request-range-interest" class="request-range-input" type="range" min="3" max="48" step="1" value="20">
                </div>
                <div style="clear: both"></div>
            </div>
        </div>
        </div>
   </div>
        <!------>
        <div id="request-form-bottomtext" class="request-form-bottomtext-1">
            <div class="request-calculation">
                  <center>
                      <ul>
                        <h6>Préstamo: <span id="request-calculation-value-1"> 10,000</span></h6>
                        <p>Importe total: <span id="request-calculation-value-3"> 114,071</span></p>

                        <div class="astSimulador">* Tipo interés medio en la web 2%</div>

                        <div class="datos-calculo">
                            <li>
                                <span>Honorarios:</span><span id="request-calculation-value-2">14,071</span>
                            </li>
                            <li>
                                <span>Fecha de vencimiento</span><span id="request-calculation-value-4">28/12/2016</span>
                            </li>
                        <!-- </div> -->
                    </ul>

                  </center>
            </div>
        </div>
        <div id="request-form-bottomtext" class="request-form-bottomtext-2">
            <div class="request-calculation">
                <center>
                    <h6>Cuota: </h6>
                    <div class="astSimulador">* Tipo interés medio en la web 2%</div>
               </center>
            </div>
        </div>
    </div>
</div>
